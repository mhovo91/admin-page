import Vue from "vue";
import Vuex from "vuex";
import { getAPI } from "../API/API";
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    posts: [],
    photos: [],
    users: [],
    autoriseUser: null,
  },

  mutations: {
    set_posts(state, posts) {
      state.posts = posts;
    },

    set_photos(state, photos) {
      state.photos = photos;
    },

    set_users(state, getUsers) {
      !state.users.some(item => item.page === getUsers.page) && state.users.push(getUsers);
    },

    set_autorise_user(state, user) {
      state.autoriseUser = user;
    },

    add_users(state, user) {
      !state.users[0].data.some(item => item.email === user.email) && state.users[0].data.push(user);
    },

    edit_users(state, user) {
      let index = state.users[0].data.indexOf(state.users[0].data.find(item => item.id === user.id));
      state.users[0].data.splice(index, 1, user);
    },

    delete_users(state, id) {
      const index = state.users[0].data.indexOf(state.users[0].data.find(item => item.id === id));
      state.users[0].data.splice(index, 1);
    },
  },

  actions: {
    fetchPhotos({ commit }) {
      getAPI("https://api.unsplash.com/photos/?client_id=vXNoeTwj2FxfQMbH-16gaa2425Y1G6aqlN4fhvt-xdI").then(data => {
        commit('set_photos', data);
      });
    },

    fetchPosts({ commit }) {
      getAPI("https://jsonplaceholder.typicode.com/posts").then(data => {
        commit('set_posts', data);
      });
    },

    fetchUsers({ commit }, page) {
      getAPI(`https://reqres.in/api/users?page=${page}`).then(data => {
        commit('set_users', data);
      });
    },

    insertAutoriseUser({ commit }, data) {
      commit('set_autorise_user', data);
    },

    addUsers({ commit }, user) {
      commit('add_users', user);
    },

    editUsers({ commit }, user) {
      commit('edit_users', user);
    },

    deleteUsers({ commit }, id) {
      commit('delete_users', id);
    },
  },

  getters: {
    getUsers(state) {
      let allUsers = state.users.flatMap(item => {
        return item.data
      });
      return allUsers;
    },
  },

  plugins: [
    createPersistedState()
  ],
});