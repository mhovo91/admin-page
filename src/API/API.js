const getAPI = async (url) => {
  const response = await fetch(url);
  const API = await response.json();
  return API;
};

export { getAPI };