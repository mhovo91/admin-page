import Vue from "vue";
import VueRouter from "vue-router";
import PhotosView from "../views/PhotosView";
import PostsView from "../views/PostsView";
import UsersView from "../views/UsersView";
import UserEditView from "../views/UserEditView";
import Login from "../components/Login";
import store from "../store/index";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Login",
    component: Login,
  },
  {
    path: "/photos",
    name: "PhotosView",
    component: PhotosView,
    meta: { requiresAuth: true }
  },
  {
    path: "/posts",
    name: "PostsView",
    component: PostsView,
    meta: { requiresAuth: true }
  },
  {
    path: "/users",
    name: "UsersView",
    component: UsersView,
    meta: { requiresAuth: true },
  },

  {
    path: "/users/edit/:id",
    component: UserEditView,
    meta: { requiresAuth: true },
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!store.state.autoriseUser) {
      next({
        path: '/',
        query: { redirect: to.fullPath }
      })
    } else {
      next()
    }
  } else {
    next()
  }
});

export default router;